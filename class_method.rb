class User
  
  def self.count
    @count ||= 0
  end
  
  def self.count=(count)
    @count += 1
  end
  
  def initialize
    self.class.count += 1 # User.count(User.count = User.count + 1)
  end
end

User.new
User.new
User.new

3.times { User.new }

p User.count
p User.singleton_methods

class Admin < User
end

Admin.new
Admin.new
Admin.new

p Admin.count

module Counter
  def count
    @count ||= 0
  end
  
  def count=(count)
    @count += 1
  end
end

class Order
  extend Counter
  
  def initialize
    self.class.count += 1
  end
end

Order.new
Order.new

p Order.count

obj = Object.new
another_obj = Object.new
def obj.foo
  puts 'bar'
end

obj.foo
p obj.singleton_methods
p another_obj.singleton_methods
