puts 'Tipagem dinâmica: '
puts "\n"
x = 100
(1..4).each do
  x *= 100_000
  puts "#{x.class} #{x}"
end

puts 'Tipagem Forte:'
a = 1
b = 'one'
# puts a + b

puts "\n"
puts "Linguage Dinâmica"
puts "\n"

class Calculator

  def method_missing(method, *args)
    result = args[0].send(args[1], args[2])
    puts "#{args[0]} #{args[1]} #{args[2]} = #{result}"
  end

end

calculator = Calculator.new
calculator.add(1, '+', 2)
calculator.sub(2, '-', 2)
calculator.div(6, '/', 2)
calculator.mult(2, '*', 2)
