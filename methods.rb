def say(message = 'Hello')
  puts message
end

say
say()

class MethodSymbol
  def +(arg)
    puts __method__
  end
  
  def -(arg)
    puts __method__
  end
  
  def cool?
    true
  end
  
  def say!
    puts "Helloooo"
  end
end

m = MethodSymbol.new
m + 1
m - 1
m.cool?
m.say!

## ???
def message(number)
  message = 'Ímpar'
  message = 'Par' if number.even?
end
p '---'
p message(1)

def sample
end

def sample(a, b)
end

def sample(a, b = 'b')
end

def sample(a, *args)
  puts args
end

sample(1, 2, 3, { a: 1 }, [1, 2], 'teste')

# Atributos nomeados
def sample(name: 'Homer', email: 'homer@simpsons.fox', **keys)
  puts name
  puts email
  puts keys
end

sample(email: 'duff@simpsons.fox', beer: 'duff')

  


