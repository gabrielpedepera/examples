string = "Hello There!"
string = 'Hello There!'
string = %(Hello There!) # Ruby Style Guide
string = %[Hello There!]
string = %{Hello There!}
string = %|Hello There!|
string = %!Hello There\!!

string = %q(Hello There! #{lang})
string = %Q(Hello There! #{lang})


lang = 'ruby'
p "Hello from #{lang}!!" # Interpolação
p 'Hello from ' + lang + '!!' # Concatenação


# heredoc (placeholder)
name = 'Homer J Simpsons'
string = <<HTML
  <p class="hello">
    Hellor There, #{name} !!
  </p>
HTML

string = <<-HTML
  <p class="hello">
    Hellor There, #{name} !!
  </p>
HTML

puts string
