items = [1, 2, 3, 4]

items.each do |item|
  puts "Item => #{item}"
end

items.each_with_index do |item, index|
  puts "Index => #{index}, Item => #{item}"
end

items.each.with_index(100) do |item, index|
  puts "Index => #{index}, Item => #{item}"
end

double = items.map do |item|
  item * 2
end
p double

double = items.collect do |item|
  item * 2
end
p double

even_only = items.select do |item|
  item.even?
end
p even_only