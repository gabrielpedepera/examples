a = [1, 2, 3, 4, 'ruby']

p a[2]
p a.include?('ruby')
p a.index(3)
p a.size # mais comun
p a.length

a.push('hello') # Adiciona no fim
a << 'world'
p a

a.unshift('Oi') # Adiciona no início
p a

p a.pop # retira do fim
p a

p a.shift # retira do início
p a


b = (1..3).to_a
c = (4..6).to_a

p b + c
p b.concat(c)

array = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
p array.flatten

p [nil, nil, 'a', 1, 2, nil].compact # remove nulos

d = [1, 2, 3]
e = [1, 2, 4, 5]

p d - e # possui em 'd', mas não em 'e'
p d | e # union
p d & e # intersection
