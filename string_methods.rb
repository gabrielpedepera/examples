# coding: utf-8
string = 'Apple - éá - '

p string.size
p string.bytesize
p string.upcase

p string[0, 6]
p string[0...6]

p string.reverse

# Palindromos
string = 'radar'
p string == string.reverse
