# IEEE-754
0.3 - 0.2 == 0.1
#=> false

0.3 - 0.2
#=> 0.09999999999999998

require 'bigdecimal'
BigDecimal('0.3') - BigDecimal('0.2') == 0.1

number = 123456789

puts number.to_s(2)
puts number.to_s(16)
puts number.to_s(36)

base36 = '7clzt'
puts base36.to_i(36)

puts 3 / 2
puts 3.0 / 2

puts 3 * 2
puts 3 + 3
puts 3 - 4

puts 3 ** 2 # potência

puts 2.even?
puts 3.odd?
puts 0.zero?

puts 3.6.floor
puts 3.6.ceil

require 'prime'
puts 7.prime?

puts 'Tipagem dinâmica: '
puts "\n"
x = 100
(1..4).each do
  x *= 100_000
  puts "#{x.class} #{x}"
end


number = 10
number2 = 10

p number.object_id == number2.object_id