class User < Object
  attr_accessor :email
  attr_writer :email
  attr_reader :email
  
  def initialize(name, email)
    self.name = name
    self.email = email
  end
  
  def name
    @name
  end
  
  def name=(name)
    @name = name.upcase
  end
end

user = User.new('homer', 'homer@simpsons.fox')
p user.name
p user.email

class Admin < User
  attr_accessor :type
  
  def initialize(name, email, type)
    super(name, email)
    self.type = type
  end
end

admin = Admin.new('Bart', 'bart@simpsons.fox', 'financial')
p admin.name
p admin.email
p admin.type